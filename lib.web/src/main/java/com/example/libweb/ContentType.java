package com.example.libweb;

public enum ContentType {
    APPLICATION_JSON,
    APPLICATION_URL_ENCODED
}
