package com.example.libweb;

import com.google.common.base.Joiner;
import com.google.gson.Gson;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Response {

    private okhttp3.Response Response;

    Response(okhttp3.Response response) {
        this.Response = response;
    }

    public Object GetStreamAs(Type type) throws IOException {
        try {
            okhttp3.ResponseBody responseBody = this.Response.body();
            if (responseBody == null)
                throw new IOException("Response body is null");

            return new Gson().fromJson(responseBody.charStream(), type);
        } finally {
            this.Response.close();
        }
    }

    public Map<String, String> GetCookies() {
        List<String> setCookies = this.Response.headers("Set-Cookie");
        String[] cookies = Joiner.on(";").skipNulls().join(setCookies).split(";");

        Map<String, String> mapHeaders = new HashMap<>();
        for (String header : cookies) {
            String[] keyValue = header.split("=");
            if (keyValue.length < 2) continue;
            mapHeaders.put(keyValue[0], keyValue[1]);
        }

        return mapHeaders;
    }
}
