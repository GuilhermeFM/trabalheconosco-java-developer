package com.example.libweb;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;


public class Request {

    private static final int MAX_TIMEOUT = 10;

    private Map<String, String> Cookies = new HashMap<>();
    private String ParametrosPost;

    private HttpUrl.Builder httpUrlBuilder;
    private OkHttpClient okHttpClient;
    private MediaType mediaType;
    private RequestBody requestBody;
    private okhttp3.Request.Builder requestBuilder;


    public Request(String url) throws MalformedURLException {
        HttpUrl httpUrl = HttpUrl.parse(url);
        if (httpUrl == null) throw new MalformedURLException();
        this.httpUrlBuilder = httpUrl.newBuilder();
        this.requestBuilder = new okhttp3.Request.Builder();

        this.okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(MAX_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(MAX_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(MAX_TIMEOUT, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build();
    }

    public Request AddCookie(String key, String value) {
        this.Cookies.put(key, value);
        return this;
    }

    public Request AddHeader(String key, String value) {
        this.requestBuilder = requestBuilder.addHeader(key, value);
        return this;
    }

    public Request AddParametroGet(String key, String value) {
        this.httpUrlBuilder.addQueryParameter(key, value);
        return this;
    }

    public Request AddParametrosPost(String value, ContentType contentType) {
        switch (contentType) {
            case APPLICATION_JSON:
                this.mediaType = MediaType.parse("application/json");
                break;
            case APPLICATION_URL_ENCODED:
                this.mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=UTF-8");
                break;
        }

        this.requestBody = RequestBody.create(mediaType, value);
        return this;
    }

    public Request AddParametrosPost(String key, String value) {
        this.ParametrosPost += String.format("%s=%s&", key, value);
        return this;
    }

    public Response ExecuteGet() throws IOException {
        if (!this.Cookies.isEmpty()) {
            String cookies = this.BuildCookies();
            this.requestBuilder = this.requestBuilder.addHeader("Cookie", cookies);
        }

        HttpUrl httpUrl = this.httpUrlBuilder.build();
        okhttp3.Request okhttpRequest = this.requestBuilder.url(httpUrl).addHeader("Connection", "close").build();
        okhttp3.Response okhttp3Response = this.okHttpClient.newCall(okhttpRequest).execute();
        return new Response(okhttp3Response);
    }

    public Response ExecutePost() throws IOException {
        if (!this.Cookies.isEmpty()) this.requestBuilder.addHeader("Cookie", this.BuildCookies());
        if (this.requestBody != null) this.requestBuilder.post(requestBody);
        else {
            this.mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=UTF-8");
            this.requestBody = RequestBody.create(mediaType, this.ParametrosPost);
            this.requestBuilder.post(this.requestBody);
        }

        HttpUrl httpUrl = this.httpUrlBuilder.build();
        okhttp3.Request okhttpRequest = this.requestBuilder.url(httpUrl).addHeader("Connection", "close").build();
        okhttp3.Response okhttp3Response = this.okHttpClient.newCall(okhttpRequest).execute();
        return new Response(okhttp3Response);
    }

    private String BuildCookies() {
        StringBuilder cookies = new StringBuilder();
        for (Map.Entry<String, String> cookie : this.Cookies.entrySet())
            cookies.append(String.format("%s=%s;", cookie.getKey(), cookie.getValue()));

        return cookies.toString();
    }
}
