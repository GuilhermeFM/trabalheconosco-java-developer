package com.example.guifr.trabalheconosco_android_developer.view;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;

import com.example.guifr.trabalheconosco_android_developer.R;
import com.example.guifr.trabalheconosco_android_developer.model.UsuarioGitHub;
import com.example.guifr.trabalheconosco_android_developer.presentation.BuscarUsuarioGitHubPresenter;

public class BuscarUsuarioGitHubActivity extends AppCompatActivity implements BuscarUsuarioGitHubPresenter.View {
    private BuscarUsuarioGitHubPresenter usuarioGitHubPresenter = new BuscarUsuarioGitHubPresenter(this);
    private AppCompatButton appCompatButton;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_usuario_github);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editText = findViewById(R.id.usuario_github);
        appCompatButton = findViewById(R.id.botao_search);
        appCompatButton.setOnClickListener(view -> {
            view.setEnabled(false);
            editText.setEnabled(false);
            usuarioGitHubPresenter.buscarUsuarioGitHub(editText.getText().toString());
        });
    }
    @Override protected void onDestroy() {
        super.onDestroy();
        usuarioGitHubPresenter.destroy();
    }

    @Override public void ativarControles() {
        appCompatButton.setEnabled(true);
        editText.setEnabled(true);
    }
    @Override public void iniciarProximaAtividade(UsuarioGitHub usuarioGitHub) {
        Intent intent = new Intent(BuscarUsuarioGitHubActivity.this, BuscarRepositoriosUsuarioGitHubActivity.class);
        intent = intent.putExtra("usuario", usuarioGitHub);
        startActivity(intent);
    }
    @Override public void mostrarErroUsuarioNaoEncontrado(Throwable throwable) {
        AlertDialog alertDialog = new AlertDialog.Builder(BuscarUsuarioGitHubActivity.this).create();
        alertDialog.setTitle("Ocorreu um erro");
        alertDialog.setMessage(throwable.getMessage());
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", (dialogInterface, i) -> dialogInterface.dismiss());
        alertDialog.show();
    }
    @Override public void mostrarErroGenerico() {
        AlertDialog alertDialog = new AlertDialog.Builder(BuscarUsuarioGitHubActivity.this).create();
        alertDialog.setTitle("Ocorreu um erro");
        alertDialog.setMessage("Ops ocorreu um erro.");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", (dialogInterface, i) -> dialogInterface.dismiss());
        alertDialog.show();
    }
}
