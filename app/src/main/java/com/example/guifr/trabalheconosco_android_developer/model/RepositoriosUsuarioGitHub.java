package com.example.guifr.trabalheconosco_android_developer.model;

import com.example.guifr.trabalheconosco_android_developer.exceptions.NenhumRepositorioEncontradoException;
import com.example.libweb.Request;
import com.example.libweb.Response;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import io.reactivex.Single;

public class RepositoriosUsuarioGitHub {
    private String name;
    private String language;

    public String getName() {
        return name;
    }
    public String getLanguage() {
        return language;
    }
}
