package com.example.guifr.trabalheconosco_android_developer.exceptions;

public class NenhumRepositorioEncontradoException extends Exception {
    public NenhumRepositorioEncontradoException() {
    }
    public NenhumRepositorioEncontradoException(String message) {
        super(message);
    }
}
