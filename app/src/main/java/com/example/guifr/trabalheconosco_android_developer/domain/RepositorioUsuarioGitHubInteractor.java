package com.example.guifr.trabalheconosco_android_developer.domain;

import com.example.guifr.trabalheconosco_android_developer.exceptions.NenhumRepositorioEncontradoException;
import com.example.guifr.trabalheconosco_android_developer.model.RepositoriosUsuarioGitHub;
import com.example.guifr.trabalheconosco_android_developer.model.UsuarioGitHub;
import com.example.libweb.Request;
import com.example.libweb.Response;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import io.reactivex.Single;

public class RepositorioUsuarioGitHubInteractor {
    public Single<List<RepositoriosUsuarioGitHub>> recuperarRepositoriosUsuarioGitHub(UsuarioGitHub usuarioGitHub) {
        return Single.create(emitter -> {
            Response response = new Request(String.format("https://api.github.com/users/%s/repos", usuarioGitHub.getLogin()))
                    .ExecuteGet();

            Type type = new TypeToken<List<RepositoriosUsuarioGitHub>>() {
            }.getType();
            List<RepositoriosUsuarioGitHub> repositoriosUsuarioGitHub = (List<RepositoriosUsuarioGitHub>) response.GetStreamAs(type);
            if (repositoriosUsuarioGitHub == null || repositoriosUsuarioGitHub.size() == 0)
                emitter.onError(new NenhumRepositorioEncontradoException(String.format("Nenhum repositorio encontrado para o uasuário %s", usuarioGitHub.getLogin())));

            emitter.onSuccess(repositoriosUsuarioGitHub);
        });
    }
}
