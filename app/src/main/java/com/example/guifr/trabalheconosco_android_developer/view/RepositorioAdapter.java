package com.example.guifr.trabalheconosco_android_developer.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.guifr.trabalheconosco_android_developer.R;
import com.example.guifr.trabalheconosco_android_developer.model.RepositoriosUsuarioGitHub;

import java.util.ArrayList;
import java.util.List;

public class RepositorioAdapter extends Adapter<RepositorioAdapter.RepositorioViewHolder> {
    private LayoutInflater layoutInflater;
    private List<RepositoriosUsuarioGitHub> repositoriosUsuarioGitHubs = new ArrayList<>();

    @NonNull
    @Override public RepositorioViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.getContext());

        return new RepositorioViewHolder(layoutInflater.inflate(R.layout.item_buscar_repositorios_usuario_github, parent, false));
    }
    @Override public void onBindViewHolder(@NonNull RepositorioViewHolder holder, int position) {
        RepositoriosUsuarioGitHub repositoriosUsuarioGitHub = repositoriosUsuarioGitHubs.get(position);
        holder.textViewRepositorio.setText(repositoriosUsuarioGitHub.getName());
        holder.textViewLinguagemRepositorio.setText(repositoriosUsuarioGitHub.getLanguage());
    }
    @Override public int getItemCount() {
        return repositoriosUsuarioGitHubs.size();
    }

    void addRepositoriosUsuarioGitHub(List<RepositoriosUsuarioGitHub> repositoriosUsuarioGitHubs) {
        this.repositoriosUsuarioGitHubs.clear();
        this.repositoriosUsuarioGitHubs.addAll(repositoriosUsuarioGitHubs);
        notifyDataSetChanged();
    }

    static class RepositorioViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewRepositorio;
        private TextView textViewLinguagemRepositorio;


        private RepositorioViewHolder(View view) {
            super(view);

            this.textViewRepositorio = view.findViewById(R.id.nome_repositorio);
            this.textViewLinguagemRepositorio = view.findViewById(R.id.nome_linguagem_repositorio);
        }
    }
}
