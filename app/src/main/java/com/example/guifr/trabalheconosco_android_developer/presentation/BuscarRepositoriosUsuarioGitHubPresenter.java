package com.example.guifr.trabalheconosco_android_developer.presentation;

import com.example.guifr.trabalheconosco_android_developer.domain.RepositorioUsuarioGitHubInteractor;
import com.example.guifr.trabalheconosco_android_developer.exceptions.NenhumRepositorioEncontradoException;
import com.example.guifr.trabalheconosco_android_developer.model.RepositoriosUsuarioGitHub;
import com.example.guifr.trabalheconosco_android_developer.model.UsuarioGitHub;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class BuscarRepositoriosUsuarioGitHubPresenter {
    private View view;
    private Disposable disposable;
    private RepositorioUsuarioGitHubInteractor interactor;

    public BuscarRepositoriosUsuarioGitHubPresenter(View view) {
        this.view = view;
        this.interactor = new RepositorioUsuarioGitHubInteractor();
    }

    public void carregarDadosRepositorio(UsuarioGitHub usuarioGitHub) {
        if (interactor == null) return;

        if (usuarioGitHub != null) {
            disposable = interactor.recuperarRepositoriosUsuarioGitHub(usuarioGitHub)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(repositorios -> {
                        if (view == null) return;
                        view.atualizarDadosRepositorios(repositorios);
                    }, throwable -> {
                        if (view == null) return;
                        if (throwable instanceof NenhumRepositorioEncontradoException) view.mostrarErroRepositorioNaoEncontrado(throwable);
                        else view.mostrarErroGenerico();
                    });
        }
    }

    public void carregarDadosUsuario(UsuarioGitHub usuarioGitHub) {
        if (usuarioGitHub != null) {
            view.atualizarNomeUsuario(usuarioGitHub.getLogin());
            view.atualizarAvatarUsuario(usuarioGitHub.getAvatar_url());
        }
    }

    public void destroy() {
        if (disposable != null && !disposable.isDisposed())
            disposable.dispose();

        this.view = null;
        this.interactor = null;
    }

    public interface View {
        void atualizarDadosRepositorios(List<RepositoriosUsuarioGitHub> repositoriosUsuarioGitHubs);
        void atualizarNomeUsuario(String nomeUsuario);
        void atualizarAvatarUsuario(String urlAvatar);
        void mostrarErroRepositorioNaoEncontrado(Throwable throwable);
        void mostrarErroGenerico();
    }
}
