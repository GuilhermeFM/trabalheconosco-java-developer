package com.example.guifr.trabalheconosco_android_developer.domain;

import com.example.guifr.trabalheconosco_android_developer.exceptions.UsuarioNaoEncontradoException;
import com.example.guifr.trabalheconosco_android_developer.model.UsuarioGitHub;
import com.example.libweb.Request;
import com.example.libweb.Response;

import io.reactivex.Single;

public class UsuarioGitHubInteractor {

    public Single<UsuarioGitHub> recuperarUsuarioGitHub(String username) {
        return Single.create(emitter -> {
            Response response = new Request(String.format("https://api.github.com/users/%s", username))
                    .ExecuteGet();

            UsuarioGitHub usuarioGitHub = (UsuarioGitHub) response.GetStreamAs(UsuarioGitHub.class);
            if (usuarioGitHub.getLogin() == null || usuarioGitHub.getLogin().isEmpty())
                emitter.onError(new UsuarioNaoEncontradoException("Usuário nao encontrado. Por favor digite outro nome de usuário"));

            emitter.onSuccess(usuarioGitHub);
        });
    }
}
