package com.example.guifr.trabalheconosco_android_developer.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.guifr.trabalheconosco_android_developer.exceptions.UsuarioNaoEncontradoException;
import com.example.libweb.Request;
import com.example.libweb.Response;

import io.reactivex.Single;

public class UsuarioGitHub implements Parcelable {
    private String login;
    private String avatar_url;

    private UsuarioGitHub(Parcel in) {
        login = in.readString();
        avatar_url = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(login);
        dest.writeString(avatar_url);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UsuarioGitHub> CREATOR = new Creator<UsuarioGitHub>() {
        @Override
        public UsuarioGitHub createFromParcel(Parcel in) {
            return new UsuarioGitHub(in);
        }

        @Override
        public UsuarioGitHub[] newArray(int size) {
            return new UsuarioGitHub[size];
        }
    };

    public String getLogin() {
        return login;
    }
    public String getAvatar_url() {
        return avatar_url;
    }
}
