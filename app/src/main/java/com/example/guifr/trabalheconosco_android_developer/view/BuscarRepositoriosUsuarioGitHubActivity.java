package com.example.guifr.trabalheconosco_android_developer.view;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.guifr.trabalheconosco_android_developer.R;
import com.example.guifr.trabalheconosco_android_developer.model.RepositoriosUsuarioGitHub;
import com.example.guifr.trabalheconosco_android_developer.presentation.BuscarRepositoriosUsuarioGitHubPresenter;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.disposables.Disposable;

public class BuscarRepositoriosUsuarioGitHubActivity extends AppCompatActivity implements BuscarRepositoriosUsuarioGitHubPresenter.View {
    private BuscarRepositoriosUsuarioGitHubPresenter buscarRepositoriosUsuarioGitHubPresenter = new BuscarRepositoriosUsuarioGitHubPresenter(this);

    private RecyclerView mRecyclerView;
    private CircleImageView mCircleImageViewAvatar;
    private TextView mTextViewUsuarioGitHub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_repositorios_usuario_github);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mRecyclerView = findViewById(R.id.repositorios_recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(new RepositorioAdapter());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL));

        mCircleImageViewAvatar = findViewById(R.id.avatar_usuario_github);
        mTextViewUsuarioGitHub = findViewById(R.id.login_usuario_github);

        buscarRepositoriosUsuarioGitHubPresenter.carregarDadosUsuario(getIntent().getParcelableExtra("usuario"));
        buscarRepositoriosUsuarioGitHubPresenter.carregarDadosRepositorio(getIntent().getParcelableExtra("usuario"));
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        buscarRepositoriosUsuarioGitHubPresenter.destroy();
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override public void atualizarDadosRepositorios(List<RepositoriosUsuarioGitHub> repositoriosUsuarioGitHubs) {
        RepositorioAdapter repositorioAdapter = (RepositorioAdapter) mRecyclerView.getAdapter();
        repositorioAdapter.addRepositoriosUsuarioGitHub(repositoriosUsuarioGitHubs);
    }
    @Override public void atualizarNomeUsuario(String nomeUsuario) {
        mTextViewUsuarioGitHub.setText(nomeUsuario);
    }
    @Override public void atualizarAvatarUsuario(String urlAvatar) {
        Glide.with(BuscarRepositoriosUsuarioGitHubActivity.this)
                .load(urlAvatar)
                .into(mCircleImageViewAvatar);
    }
    @Override public void mostrarErroRepositorioNaoEncontrado(Throwable throwable) {
        AlertDialog alertDialog = new AlertDialog.Builder(BuscarRepositoriosUsuarioGitHubActivity.this).create();
        alertDialog.setMessage(throwable.getMessage());
        alertDialog.setTitle("Ocorreu um erro");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", (dialogInterface, i) -> dialogInterface.dismiss());
        alertDialog.show();
    }
    @Override public void mostrarErroGenerico() {
        AlertDialog alertDialog = new AlertDialog.Builder(BuscarRepositoriosUsuarioGitHubActivity.this).create();
        alertDialog.setTitle("Ocorreu um erro");
        alertDialog.setMessage("Ops ocorreu um erro.");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", (dialogInterface, i) -> dialogInterface.dismiss());
        alertDialog.show();
    }
}
