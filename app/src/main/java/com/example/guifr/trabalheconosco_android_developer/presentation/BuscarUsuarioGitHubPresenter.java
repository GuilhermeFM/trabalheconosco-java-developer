package com.example.guifr.trabalheconosco_android_developer.presentation;

import com.example.guifr.trabalheconosco_android_developer.domain.UsuarioGitHubInteractor;
import com.example.guifr.trabalheconosco_android_developer.exceptions.UsuarioNaoEncontradoException;
import com.example.guifr.trabalheconosco_android_developer.model.UsuarioGitHub;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class BuscarUsuarioGitHubPresenter {
    private View view;
    private Disposable disposable;
    private UsuarioGitHubInteractor interactor;

    public BuscarUsuarioGitHubPresenter(View view) {
        this.view = view;
        this.interactor = new UsuarioGitHubInteractor();
    }

    public void buscarUsuarioGitHub(String username) {
        if (interactor == null) return;

        disposable = interactor.recuperarUsuarioGitHub(username)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(usuario -> {
                    if (view == null) return;

                    view.ativarControles();
                    view.iniciarProximaAtividade(usuario);
                }, throwable -> {
                    if (view == null) return;

                    view.ativarControles();
                    if (throwable instanceof UsuarioNaoEncontradoException)
                        view.mostrarErroUsuarioNaoEncontrado(throwable);
                    else
                        view.mostrarErroGenerico();
                });
    }

    public void destroy() {
        if (disposable != null && !disposable.isDisposed())
            disposable.dispose();

        this.view = null;
        this.interactor = null;
    }

    public interface View {
        void ativarControles();
        void iniciarProximaAtividade(UsuarioGitHub usuarioGitHub);
        void mostrarErroUsuarioNaoEncontrado(Throwable throwable);
        void mostrarErroGenerico();
    }
}
